# bashdb

bashdb is a database written in Bash. It is very primitive but light, works *most of the time* and supports IO Locking. Forks are welcomed, as long as credited properly.

### A few things to keep in mind

- To avoid disaster, the delimiter (`,` default) and the wrappers (`"` default) are removed when added. Keep this in mind.
- I know, this looks like a `.csv` parser, it's not. You can use it to parse `.csv` files with the right delimiter and wrapper, but it's more than that.
- The script makes a `.csv` (please I beg you read above) in the local directory for the database file.
- The script also reads a `.cfg` file (example, if file is `table.csv`, then config file is `table.cfg`) in the working directory.
- You can also copy the `bashdb` script to a folder in `$PATH` for convenience.
- Do pay attention that this script will EXIT when it sees another script working. 
- If scripts aren't working, you can try running them with `bashdb --unsafe`.
- Fails at the moment, too lazy to fix it tbh

### Usage

* `bashdb remove "dbname"` # Removes database
* `bashdb print "dbname"` # Prints database file raw
* `bashdb mkconfig "dbname"` # Generates config, optional
* `bashdb from "dbname" --find column:"string"`
    * Example: `bashdb from testdb --find 1:"test test"`
* `bash from "dbname" --find --parse column:"string"`
    * Example: `bashdb from testdb --find --parse 1:"test test`
* `bashdb from "dbname" --remove column:"string"`
    * Example: `bashdb from testdb --remove 2:"test test"`
* `bashdb from "dbname" --add "$1" "$2"`
    * Example: `bashdb from testdb --add "first column" "second column"`

	
* Config:
    * Variables:
        * `DELIMITER`: defines delimiter, default `,`
        * `WRAPPER`: defines separator, default `"`


### Examples

```
$  ./bashdb create testdb
Creating database in /home/diamond/Scripts/bashdb/testdb.csv
$  ./bashdb from testdb --add "$(date +%s)" "x said something"
"1530350790","x said something"
$  ./bashdb from testdb --add "$(date +%s)" "y replied back that x is gay"
"1530350790","y replied back that x is gay"
$  ./bashdb from testdb --add "$(date +%s)" "x retaliates with no u!"
"1530350790","x retaliates with no u!"
$  ./bashdb from testdb --add "$(date +%s)" "it's super effective!"
"1530350790","it's super effective!"
$  ./bashdb from testdb --find 2:"x is gay"
"1530350790","y replied back that x is gay"
$  ./bashdb from testdb --find --parse 2:"x is gay"
"y replied back that x is gay"
$  ./bashdb remove testdb
Removed /home/diamond/Scripts/bashdb/testdb.csv
$  ./bashdb from testdb --add "$(date +%s)"
IO LOCK TRIGGERED! IO DETECTED! STOPPING.
```

```
$  bashdb create testdb
Creating database in /testdb.csv
$  bashdb mkconfig testdb
Initiating mkconfig wizard...
What would you want your delimiter to be? [string, 1 character, default `,`] ;
DELIMITER=';'
What would you want your wrapper to be? [string, 1 character, default `"`] ~
WRAPPER='~'
$  bashdb from testdb --add "This is a test 1" "Second column here" "What about the third?"
~This is a test 1~;~Second column here~;~What about the third?~
$  bashdb from testdb --add "This is a test 2" "Second column still here" "The third is not here though."
~This is a test 2~;~Second column still here~;~The third is not here though.~
$  bashdb from testdb --find 2:"here"
~This is a test 1~;~Second column here~;~What about the third?~
~This is a test 2~;~Second column still here~;~The third is not here though.~
$  bashdb from testdb --find --parse 2:"here"
~Second column here~
~Second column still here~

# By this point you can just use your Bash skills to remove the first and last character and throw this into an array
```

### Todo
- [x] Complete help

### notes ~~taken from https://github.com/tlrobinson/wwwoosh/~~
hopefully it's obvious, but these projects are for fun and not meant to be taken seriously. wwwoosh can only handle about 2 request per second (any additional fail completely), not to mention there's probably some pretty nasty security issues with it.

it is, however, a demonstration of the simplicity of HTTP, and the power of unix shells

